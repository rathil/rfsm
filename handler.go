package rfsm

// NewHandler - get new handler
func NewHandler(transitionId TransitionId) Handler {
	return Handler{
		transitionId: transitionId,
	}
}

// NewHandlerGlobal - global handler non-associated with a transition
func NewHandlerGlobal() Handler {
	return Handler{}
}

type Handler struct {
	// Handler id
	transitionId TransitionId
	// Priority of handler
	priority int
	// Called before entering state
	beforeEnter EventCB
	// Called after entering state
	afterEnter EventCB
}

// Priority - set handler priority
func (a Handler) Priority(priority int) Handler {
	a.priority = priority
	return a
}

// BeforeEnter - set handler "before entering state". Executed in a "transaction" while FSM is locked out for changes.
func (a Handler) BeforeEnter(cb EventCB) Handler {
	a.beforeEnter = cb
	return a
}

// AfterEnter - set handler "after entering state". Executed after a change of state, when the FSM is unlocked for changes.
func (a Handler) AfterEnter(cb EventCB) Handler {
	a.afterEnter = cb
	return a
}
