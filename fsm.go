package rfsm

import (
	"fmt"
	"log"
	"sort"
	"sync"
)

var (
	ErrorSimilarState       = fmt.Errorf(`similar state has already been added`)
	ErrorNotFoundTransition = fmt.Errorf(`not found available transition`)
	ErrorLoad               = fmt.Errorf(`load new FSM`)
	ErrorSave               = fmt.Errorf(`save FSM`)
)

type Saver interface {
	Save(DataFSM) error
}
type Loader interface {
	Load() (*DataFSM, error)
}

// New FSM by default state
func New(stateId StateId) *FSM {
	return &FSM{
		stateId:        stateId,
		states:         map[stateKey]*State{},
		statesHandlers: map[stateKey]Handler{},
		handlers:       map[TransitionId]HandlerQueue{},
	}
}

// NewByFSM - new FSM by FSM (like clone)
func NewByFSM(fsm *FSM) *FSM {
	fsm.locker.RLock()
	defer fsm.locker.RUnlock()

	return &FSM{
		stateId:        fsm.stateId,
		states:         fsm.states,
		statesHandlers: fsm.statesHandlers,
		handlers:       fsm.handlers,
	}
}

// NewByLoader - get new FSM by loader
func NewByLoader(loader Loader) (*FSM, error) {
	data, err := loader.Load()
	if err != nil {
		return nil, fmt.Errorf(`%w, err: %s`, ErrorLoad, err)
	}
	fsm := New(data.StateId)
	for _, dataState := range data.States {
		if err := fsm.AddState(NewStateByDataState(dataState)); err != nil {
			return nil, err
		}
	}
	return fsm, nil
}

// FSM - finite state machine
type FSM struct {
	// Current state id
	stateId StateId
	// Available states
	states map[stateKey]*State
	// State handlers
	statesHandlers map[stateKey]Handler
	// Global handlers
	handlers map[TransitionId]HandlerQueue
	// Locker
	locker sync.RWMutex
	// Additional data
	data interface{}
}

type DataFSM struct {
	// Current state id
	StateId StateId
	// Available states
	States []DataState
}

type stateKey struct {
	transitionId TransitionId
	stateId      StateId
}

// SetData - set additional data
func (a *FSM) SetData(data interface{}) *FSM {
	a.locker.Lock()
	a.data = data
	a.locker.Unlock()

	return a
}

// GetData - get additional data
func (a *FSM) GetData() interface{} {
	a.locker.RLock()
	defer a.locker.RUnlock()

	return a.data
}

// AddState - add state
func (a *FSM) AddState(states ...State) error {
	a.locker.Lock()
	defer a.locker.Unlock()

	for _, state := range states {
		for _, from := range state.from {
			key := stateKey{state.transitionId(), from}
			if _, ok := a.states[key]; ok {
				return fmt.Errorf(
					`can't add state "%v" with transition from "%v", err: %w`,
					key.transitionId,
					key.stateId,
					ErrorSimilarState,
				)
			}
			a.states[key] = &state
			a.statesHandlers[key] = state.handler
		}
	}
	return nil
}

// MustAddState is like AddState but log.Fatal if the state can't be added
func (a *FSM) MustAddState(states ...State) *FSM {
	if err := a.AddState(states...); err != nil {
		log.Fatal(err)
	}
	return a
}

// AddHandler - add handler
func (a *FSM) AddHandler(handlers ...Handler) *FSM {
	a.locker.Lock()
	defer a.locker.Unlock()

	for _, handler := range handlers {
		if _, ok := a.handlers[handler.transitionId]; !ok {
			a.handlers[handler.transitionId] = HandlerQueue{}
		}
		a.handlers[handler.transitionId] = append(a.handlers[handler.transitionId], handler)
	}
	return a
}

// State - get current state of the FSM
func (a *FSM) State() StateId {
	a.locker.RLock()
	defer a.locker.RUnlock()

	return a.stateId
}

// IsState - check is this state is current
func (a *FSM) IsState(stateId StateId) bool {
	a.locker.RLock()
	defer a.locker.RUnlock()

	return a.stateId == stateId
}

// CanTransit - can transit to state
func (a *FSM) CanTransit(transitionId TransitionId) bool {
	a.locker.RLock()
	defer a.locker.RUnlock()

	key := stateKey{transitionId, a.stateId}
	_, ok := a.states[key]
	return ok
}

// AvailableTransitions - get available transitions in the current state
func (a *FSM) AvailableTransitions() []TransitionId {
	a.locker.RLock()
	aStates := a.states
	stateId := a.stateId
	a.locker.RUnlock()

	var transitionIds []TransitionId
	for key := range aStates {
		if key.stateId == stateId {
			transitionIds = append(transitionIds, key.transitionId)
		}
	}
	return transitionIds
}

// Transit to state
func (a *FSM) Transit(transitionId TransitionId, args ...interface{}) error {
	a.locker.Lock()

	key := stateKey{transitionId, a.stateId}
	transition, ok := a.states[key]
	if !ok {
		a.locker.Unlock()
		return fmt.Errorf(
			`can't transiting to "%v", err: %w`,
			transitionId,
			ErrorNotFoundTransition,
		)
	}
	event := Event{
		FSM:  a,
		From: a.stateId,
		To:   transition.stateId,
		Args: args,
	}
	handlerQueue := a.handlerQueue(key)
	if err := handlerQueue.beforeEnter(event); err != nil {
		a.locker.Unlock()
		return err
	}
	a.stateId = transition.stateId
	a.locker.Unlock()

	return handlerQueue.afterEnter(event)
}

func (a *FSM) handlerQueue(key stateKey) HandlerQueue {
	var handlerQueue HandlerQueue
	a.collectHandlers(&handlerQueue, a.statesHandlers[key])
	if handlers, ok := a.handlers[key.transitionId]; ok {
		for _, handler := range handlers {
			a.collectHandlers(&handlerQueue, handler)
		}
	}
	if handlers, ok := a.handlers[nil]; ok {
		for _, handler := range handlers {
			a.collectHandlers(&handlerQueue, handler)
		}
	}
	sort.Sort(handlerQueue)
	return handlerQueue
}

func (a *FSM) collectHandlers(handlerQueue *HandlerQueue, handler Handler) {
	if handler.beforeEnter != nil || handler.afterEnter != nil {
		*handlerQueue = append(*handlerQueue, handler)
	}
}

// Load new FSM
func (a *FSM) Load(loader Loader) (*FSM, error) {
	fsm, err := NewByLoader(loader)
	if err != nil {
		return nil, err
	}

	a.locker.RLock()
	fsm.statesHandlers = a.statesHandlers
	fsm.handlers = a.handlers
	a.locker.RUnlock()

	return fsm, nil
}

// MustLoad is like Load but log.Fatal if the FSM can't be load
func (a *FSM) MustLoad(loader Loader) *FSM {
	fsm, err := a.Load(loader)
	if err != nil {
		log.Fatal(err)
	}
	return fsm
}

// Save FSM
func (a *FSM) Save(saver Saver) error {
	a.locker.RLock()
	aStates := a.states
	stateId := a.stateId
	a.locker.RUnlock()

	var states []DataState
	statesUnique := map[*State]bool{}
	for _, state := range aStates {
		if !statesUnique[state] {
			statesUnique[state] = true
			states = append(states, state.getData())
		}
	}

	if err := saver.Save(DataFSM{
		StateId: stateId,
		States:  states,
	}); err != nil {
		return fmt.Errorf(`%w, err: %s`, ErrorSave, err)
	}
	return nil
}

// MustSave is like Save but log.Fatal if the FSM can't be save
func (a *FSM) MustSave(saver Saver) *FSM {
	if err := a.Save(saver); err != nil {
		log.Fatal(err)
	}
	return a
}
