# GO-lang finite-state machine

## Installation

1. Install rFSM package:

```sh
$ go get -u gitlab.com/rathil/rfsm
```

2. Import it in code:

```go
import "gitlab.com/rathil/rfsm"
```

## Example

### Initialization

```go
fsm := rfsm.New("draft").
    MustAddState(rfsm.NewState("open", "opened", "draft")).
    MustAddState(rfsm.NewState("close", "closed", "opened", "approved", "rejected")).
    MustAddState(rfsm.NewState("close", "closed", "draft").SetHandler(
        rfsm.NewHandlerGlobal().
            Priority(6).
            BeforeEnter(func(event rfsm.Event) error {
                fmt.Printf("Before (local) transit from draft to %s with args %s\n", event.To, event.Args)
                return nil
            }).
            AfterEnter(func(event rfsm.Event) error {
                fmt.Printf("After (local) transit from %s to closed with args %s\n", event.From, event.Args)
                return nil
            }),
    )).
    MustAddState(rfsm.NewState("approve", "approved").AddFrom("opened").AddFrom("draft")).
    MustAddState(rfsm.NewState("reject", "rejected", "opened", "approved", "draft")).
    AddHandler(
        rfsm.NewHandler("close").
            Priority(1).
            BeforeEnter(func(event rfsm.Event) error {
                fmt.Printf("Before transit from %s to %s with args %s\n", event.From, event.To, event.Args)
                return nil
            }).
            AfterEnter(func(event rfsm.Event) error {
                fmt.Printf("After transit from %s to %s with args %s\n", event.From, event.To, event.Args)
                return nil
            }),
    )
```

### Initialization by loader

```go
fsm, err := rfsm.NewByLoader(rfsm_json.NewLoader(strings.NewReader(`{"StateId":"draft","States":[{"TransitionId":"close","StateId":"closed","From":["draft","other_state"]}]}`)))
if err != nil {
    panic(err)
}
```

### Initialization by loader with pre-installed handlers

```go
fsm := rfsm.New("draft").
    AddHandler(
        rfsm.NewHandler("close").
            Priority(1).
            BeforeEnter(func(event rfsm.Event) error {
                fmt.Printf("Before transit from %s to %s with args %s\n", event.From, event.To, event.Args)
                return nil
            }).
            AfterEnter(func(event rfsm.Event) error {
                fmt.Printf("After transit from %s to %s with args %s\n", event.From, event.To, event.Args)
                return nil
            }),
    )

fsmNew, err := fsm.Load(rfsm_json.NewLoader(strings.NewReader(`{"StateId":"draft","States":[{"TransitionId":"close","StateId":"closed","From":["draft","other_state"]}]}`)))
if err != nil {
	panic(err)
}
fmt.Println(fsmNew.AvailableTransitions())
```

### Save FSM

```go
buf := bytes.NewBuffer(nil)
if err := fsm.Save(rfsm_json.NewSaver(buf)); err != nil {
    panic(err)
}
fmt.Println(buf.String())
```

### Get current FSM state id

```go
fsm.State()
```

### Check is this state is current

```go
fsm.IsState("closed")
```

### Get available transitions in the current state

```go
fsm.AvailableTransitions()
```

### Check available transition

```go
fsm.CanTransit("close")
```

### Transit

```go
fsm.Transit("close", "param", true, SomeStruct{})
```
