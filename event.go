package rfsm

type EventCB func(Event) error

type Event struct {
	// Current FSM
	FSM *FSM
	// State id before the handler
	From StateId
	// State id after the handler
	To StateId
	// Handler arguments
	Args []interface{}
}
