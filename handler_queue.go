package rfsm

type HandlerQueue []Handler

func (a HandlerQueue) beforeEnter(event Event) error {
	for _, handler := range a {
		if handler.beforeEnter != nil {
			if err := handler.beforeEnter(event); err != nil {
				return err
			}
		}
	}
	return nil
}
func (a HandlerQueue) afterEnter(event Event) error {
	for _, handler := range a {
		if handler.afterEnter != nil {
			if err := handler.afterEnter(event); err != nil {
				return err
			}
		}
	}
	return nil
}

func (a HandlerQueue) Len() int           { return len(a) }
func (a HandlerQueue) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a HandlerQueue) Less(i, j int) bool { return a[i].priority > a[j].priority }
