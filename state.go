package rfsm

type TransitionId interface{}
type StateId interface{}

func NewState(transitionId TransitionId, stateId StateId, from ...StateId) State {
	return State{
		handler: NewHandler(transitionId),
		stateId: stateId,
		from:    from,
	}
}

func NewStateByDataState(data DataState) State {
	return NewState(
		data.TransitionId,
		data.StateId,
		data.From...,
	)
}

type State struct {
	handler Handler
	// State id
	stateId StateId
	// List of states ids from which can go to this state
	from []StateId
}

type DataState struct {
	// Transition id
	TransitionId TransitionId
	// State id
	StateId StateId
	// List of states ids from which can go to this state
	From []StateId
}

// Get transitionId
func (a State) transitionId() TransitionId {
	return a.handler.transitionId
}

// Get data for save
func (a State) getData() DataState {
	return DataState{
		TransitionId: a.handler.transitionId,
		StateId:      a.stateId,
		From:         a.from,
	}
}

// AddFrom - add from states
func (a State) AddFrom(from ...StateId) State {
	a.from = append(a.from, from...)
	return a
}

// SetHandler - set handler
func (a State) SetHandler(handler Handler) State {
	transitionId := a.handler.transitionId
	a.handler = handler
	a.handler.transitionId = transitionId
	return a
}
