package rfsm_json

import (
	"encoding/json"
	"gitlab.com/rathil/rfsm"
	"io"
)

// NewSaver - JSON saver to raw bytes data
func NewSaver(writer io.Writer) rfsm.Saver {
	return &saver{
		encoder: json.NewEncoder(writer),
	}
}

type saver struct {
	encoder *json.Encoder
}

func (a saver) Save(data rfsm.DataFSM) error {
	return a.encoder.Encode(data)
}
