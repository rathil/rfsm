package rfsm_json

import (
	"encoding/json"
	"gitlab.com/rathil/rfsm"
	"io"
)

// NewLoader - JSON loader
func NewLoader(reader io.Reader) rfsm.Loader {
	return &load{
		decoder: json.NewDecoder(reader),
	}
}

type load struct {
	decoder *json.Decoder
}

func (a load) Load() (*rfsm.DataFSM, error) {
	data := rfsm.DataFSM{}
	if err := a.decoder.Decode(&data); err != nil {
		return nil, err
	}
	return &data, nil
}
